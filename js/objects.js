function objectModels(){
    return [
       /* {name:"models/moshak.OBJ",
        position:{x:0,y:0,z:0},texture:"./images/mooshak.jpeg",obj:null,selected:false,radius:15},
        
       {name:"models/merikh.OBJ",
        position:{x:70,y:-40,z:-300},texture:"./images/merikh.jpeg",obj:null,selected:false,radius:15},
		
		{name:"models/khorshid.OBJ",
        position:{x:-140,y:40,z:-350},texture:"./images/khorshid.jpeg",obj:null,selected:false,radius:15},*/
		 {name:"models/moshak.OBJ",
        position:{x:-100,y:-180,z:0},texture:"./images/PngItem_1268553.png",obj:null,selected:false,radius:15},
       
       {name:"models/merikh.OBJ",
        position:{x:90,y:180,z:-100},texture:"./images/Daco_4292531.png",obj:null,selected:false,radius:15},
		
		{name:"models/khorshid.OBJ",
        position:{x:-70,y:-100,z:-150},texture:"./images/kissclipart-binoculars-icon-clipart-binoculars-computer-icons-4b5167d135847e9c.png",obj:null,selected:false,radius:15},
		
		{name:"models/khorshid.OBJ",
        position:{x:60,y:100,z:-200},texture:"./images/NicePng_plant-icon-png_3345756.png",obj:null,selected:false,radius:15},
		
		{name:"models/khorshid.OBJ",
        position:{x:80,y:0,z:-350},texture:"./images/wall-clock-18.png",obj:null,selected:false,radius:15},
		
		{name:"models/khorshid.OBJ",
        position:{x:-80,y:0,z:-200},texture:"./images/pngkey.com-vhs-png-561945.png",obj:null,selected:false,radius:15},
		
		{name:"models/khorshid.OBJ",
        position:{x:-60,y:100,z:-150},texture:"./images/pngwing.com.png",obj:null,selected:false,radius:15},
		{name:"models/khorshid.OBJ",
        position:{x:70,y:-100,z:-100},texture:"./images/pngkey.com-wall-e-png-1609653.png",obj:null,selected:false,radius:15},
    ]
}

function positions(index){
    switch(index)
    {
            case 0:
            return {x:-100,y:180,z:0}
            case 1:
            return {x:90,y:150,z:-100}
            case 2:
            return {x:-70,y:-100,z:-150}
            case 3:
            return {x:60,y:100,z:-200}
            case 4:
            return {x:80,y:0,z:-350}
            case 5:
            return {x:-80,y:0,z:-200}
            case 6:
            return {x:-60,y:100,z:-150}
            case 7:
            return {x:70,y:-100,z:-100}
            case 8:
			return {x:100,y:180,z:0}
			case 9:
			return {x:-70,y:150,z:0}
           
    }

}

function scales(index){
   return 8
}



function cameraDistanceDelta(index)
{
    switch(index)
    {
            case 0: case 1:  case 2:
            return -0.5
            case 3: case 4: case 5:
            return -5
            case 6: case 7: case 8:  
            return 10
            case 9: case 10: 
            return 15
               
            
    } 
}